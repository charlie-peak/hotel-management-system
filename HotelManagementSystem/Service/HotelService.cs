﻿using HotelManagementSystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelManagementSystem.Service
{
    public class HotelService
    {
        int sequence = 0;
        private Hotel hotel = new Hotel();

        public void CreateHotel(int floor, int room)
        {
            hotel = new Hotel(room, floor);
            Console.WriteLine($"{++sequence}.) Hotel created with {floor} floor(s), {room} room(s) per floor.");
        }
        public void Book(string roomNumber, string guestName, int guestAge)
        {
            Room room = hotel.GetAvailableBookingRoomByRoomNumber(roomNumber);
            Guest guest = new Guest(guestName, guestAge);
            if (room != null)
            {
                KeyCard keyCard = hotel.GetAvailableKeyCard();
                room.CheckIn(guest, keyCard);

                Console.WriteLine($"{++sequence}.) Room {room.roomNumber} is booked by {guest.firstName} with keycard number {keyCard.keyNumber}.");
            }
            else
            {
                Room bookedRoom = hotel.GetRoomByRoomNumber(roomNumber);
                string bookedGuestName = bookedRoom.GetGuestName();
                Console.WriteLine($"{++sequence}.) Cannot book room {bookedRoom.roomNumber} for {guest.firstName}, The room is currently booked by {bookedGuestName}.");
            }
        }
        public void ListAvailableRooms()
        {
            List<string> availableRoom = hotel.ListAvailableRoom();
            Console.WriteLine($"{++sequence}.) {string.Join(' ', availableRoom)}");
        }
        public void CheckOut(string keyNumber, string guestName)
        {
            KeyCard keyCard = hotel.GetKeyCardByKeyCardNumber(keyNumber);
            if (keyCard != null)
            {
                Room room = hotel.GetRoomByRoomNumber(keyCard.roomNumber);
                if (room.GetGuestName() == guestName)
                {
                    room.CheckOut(keyCard);
                    Console.WriteLine($"{++sequence}.) Room {room.roomNumber} is checkout.");
                }
                else
                {
                    Console.WriteLine($"{++sequence}.) Only {room.GetGuestName()} can checkout with keycard number {keyCard.keyNumber}.");
                }
            }
            else
            {
                Console.WriteLine($"{++sequence}.) keyCard not found, Can not checkout.");
            }
        }
        public void ListGuest() {
            List<Guest> guestModelList = hotel.GetGuestList();
            if (guestModelList != null)
            {
                Console.WriteLine($"{++sequence}.) {string.Join(", ", guestModelList.Select(x => x.firstName))}");
            }
            else
            {
                Console.WriteLine($"list guest not found.");
            }
        }
        public void GetGuestInRoom(string roomNumber) 
        {
            Room room = hotel.GetRoomByRoomNumber(roomNumber);
            if (room != null)
            {
                string guestName = room.GetGuestName();
                Console.WriteLine($"{++sequence}.) {guestName}.");
            }
            else
            {
                Console.WriteLine($"{++sequence}.) Room Number not found.");
            }
        }
        public void ListGuestByAge(int age, string option) 
        {
            bool isGreaterThan = option == ">"; 
            List<Guest> guestModelList = hotel.GetGuestListFindByAge(age, isGreaterThan);
            if (guestModelList != null)
            {
                Console.WriteLine($"{++sequence}.) {string.Join(", ", guestModelList.Select(x => x.firstName))}");
            }
            else
            {
                Console.WriteLine($"list guest not found.");
            }
        }
        public void ListGuestByFloor(int floor) 
        {
            List<Guest> guestModelList = hotel.GetGuestListFindByFloor(floor);
            if (guestModelList != null)
            {
                Console.WriteLine($"{++sequence}.) {string.Join(", ", guestModelList.Select(x => x.firstName))}");
            }
            else
            {
                Console.WriteLine($"list guest not found.");
            }
        }
        public void CheckOutGuestByFloor(int floor) 
        {
            List<Room> rooms = hotel.GetRoomByFloor(floor);
            List<string> keyCardNumberList = new List<string>();
            List<string> roomNumberList = new List<string>();

            foreach (Room room in rooms?.Where(x => !x.isAvailable) ?? (new List<Room>()))
            {
                string keyCardRoom = room.GetKeyCardNumber();
                KeyCard keyCard = hotel.GetKeyCardByKeyCardNumber(keyCardRoom);

                room.CheckOut(keyCard);
                keyCardNumberList.Add(keyCard.keyNumber);
                roomNumberList.Add(room.roomNumber);
            }

            if ((keyCardNumberList?.Any() ?? false) && (roomNumberList?.Any() ?? false))
            {
                Console.WriteLine($"{++sequence}.) Room {string.Join(",", roomNumberList)} are checkout with keycard number {string.Join(",", keyCardNumberList)}.");
            }
            else
            {
                Console.WriteLine($"{++sequence}.) Room by floor not found.");
            }
        }
        public void BookByFloor() { }
    }
}
