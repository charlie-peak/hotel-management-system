﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotelManagementSystem.Model
{
    public class Guest
    {
        public string firstName { get; set; }
        public int age { get; set; }
        public Guest(string firstName, int age)
        {
            this.firstName = firstName;
            this.age = age;
        }
    }
}
