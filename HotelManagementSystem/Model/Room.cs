﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelManagementSystem.Model
{
    public class Room
    {
        public int floor { get; set; }
        public string roomNumber { get; set; }
        public bool isAvailable { get; set; }
        public List<string> keyCardNumberList { get; set; }
        public Guest guest { get; set; }

        public Room(int floor, string roomNumber)
        {
            this.floor = floor;
            this.roomNumber = roomNumber;
            this.isAvailable = true;
        }
        public void CheckIn(Guest bookedGuest, KeyCard keyCard)
        {
            isAvailable = false;
            guest = bookedGuest;
            keyCardNumberList = new List<string>
            {
                keyCard?.keyNumber
            };
            keyCard.SetKeyCard(roomNumber);
        }
        public void CheckOut(KeyCard keyCard)
        {
            isAvailable = true;
            keyCardNumberList = new List<string>();
            guest = null;
            keyCard.UnSetKeyCard();
        }
        public string GetKeyCardNumber()
        {
            string keyCardNumber = keyCardNumberList.FirstOrDefault();
            return keyCardNumber;
        }
        public string GetGuestName()
        {
            if(guest != null)
            {
                return guest.firstName;
            }
            return string.Empty;
        }
    }
}
