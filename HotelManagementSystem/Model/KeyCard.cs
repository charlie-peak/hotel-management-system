﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotelManagementSystem.Model
{
    public class KeyCard
    {
        public string keyNumber { get; set; }
        public string roomNumber { get; set; }

        public KeyCard(string keyNumber)
        {
            this.keyNumber = keyNumber;
        }
        public void SetKeyCard(string roomNumber)
        {
            this.roomNumber = roomNumber;
        }
        public void UnSetKeyCard()
        {
            this.roomNumber = string.Empty;
        }
    }
}
