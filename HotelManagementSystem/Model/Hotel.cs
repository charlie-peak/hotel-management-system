﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelManagementSystem.Model
{
    public class Hotel
    {
        List<Room> roomList { get; set; }
        List<KeyCard> keyList { get; set; }

        public Hotel()
        {
            roomList = new List<Room>();
            keyList = new List<KeyCard>();
        }
        public Hotel(int room, int floor)
        {
            roomList = new List<Room>();
            keyList = new List<KeyCard>();
            if (room > 0 && floor > 0)
            {
                int roomCard = 1;
                for (int countFloor = 1; floor >= countFloor; countFloor++)
                {
                    for (int countRoom = 1; room >= countRoom; countRoom++)
                    {
                        string roomNumber = countFloor.ToString();
                        if (countRoom < 10)
                        {
                            roomNumber += "0";
                        }
                        roomNumber += countRoom.ToString();
                        AddKeyCard();
                        roomList.Add(new Room(countFloor, roomNumber));
                        roomCard++;
                    }
                }
            }
        }
        public void AddKeyCard()
        {
            int keyNumber = keyList.Count + 1;
            keyList.Add(new KeyCard(keyNumber.ToString()));
        }
        public List<string> ListAvailableRoom()
        {
            List<string> result = roomList.Where(x => x.isAvailable).Select(x => x.roomNumber).ToList();
            return result;
        }

        public List<Room> GetAvailableBookingRoom()
        {
            List<Room> rooms = this.roomList.Where(x => x.isAvailable).ToList();
            return rooms;
        }
        public List<Room> GetAvailableBookingRoomByFloor(int floor)
        {
            List<Room> rooms = roomList.Where(x => x.isAvailable && x.floor == floor).ToList();
            return rooms;
        }
        public Room GetAvailableBookingRoomByRoomNumber(string roomNumber)
        {
            Room room = roomList.FirstOrDefault(x => x.isAvailable && x.roomNumber == roomNumber);
            return room;
        }

        public List<Room> GetRoomByFloor(int floor)
        {
            List<Room> rooms = roomList.Where(x => x.floor == floor).ToList();
            return rooms;
        }
        public Room GetRoomByRoomNumber(string roomNumber)
        {
            Room room = roomList.FirstOrDefault(x => x.roomNumber == roomNumber);
            return room;
        }
       
        public KeyCard GetAvailableKeyCard()
        {
            List<KeyCard> keyCardModelList = keyList.Where(x => string.IsNullOrEmpty(x.roomNumber)).ToList();
            KeyCard keyCard = null;
            if (keyCardModelList?.Any() == true) 
            {
                keyCard = keyCardModelList.OrderBy(x => x.keyNumber).FirstOrDefault();
            }
             
            return keyCard;
        }
        public KeyCard GetKeyCardByKeyCardNumber(string keyCardNumber)
        {
            KeyCard keyCard = keyList.FirstOrDefault(x => x.keyNumber == keyCardNumber);
            return keyCard;
        }

        public List<Guest> GetGuestList()
        {
            List<Guest> guestModelList = roomList.Where(x => !x.isAvailable).Select(x => x.guest).ToList();
            return guestModelList;
        }
        public List<Guest> GetGuestListFindByAge(int age, bool isGreaterThan)
        {
            List<Guest> guestModelList = GetGuestList();
            if (guestModelList?.Any() == true)
            {
                guestModelList = isGreaterThan ? guestModelList.Where(x => x.age >= age).ToList() : guestModelList.Where(x => x.age < age).ToList();
            }
            return guestModelList;
        }
        public List<Guest> GetGuestListFindByFloor(int floor)
        {
            List<Guest> guestModelList = roomList.Where(x => x.floor == floor && x.guest != null).Select(x => x.guest).ToList();
            return guestModelList;
        }
    }
}
