﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using HotelManagementSystem.Model;
using HotelManagementSystem.Service;

namespace HotelManagementSystem
{
    class Program
    {
        static HotelService hotelService = new HotelService();
        static int count = 0;
        static Hotel hotel = null;

        static void Main(string[] args)
        {
            string[] lines = File.ReadAllLines(Environment.CurrentDirectory + @"\Resource\input.txt", Encoding.UTF8);
            foreach (string line in lines)
            {
                HotelManagementCommand(++count, line);
            }
        }

        private static void HotelManagementCommand(int sequence, string input)
        {
            string[] command = input.Split(' ');
            switch (command[0])
            {
                case "create_hotel":
                    {
                        int floor = int.Parse(command[1]);
                        int room = int.Parse(command[2]);

                        hotelService.CreateHotel(floor, room);
                        break;
                    }
                case "book":
                    {
                        string roomNumber = command[1];
                        string guestName = command[2];
                        int guestAge = int.Parse(command[3]);

                        hotelService.Book(roomNumber, guestName, guestAge);
                        break;
                    }
                case "list_available_rooms":
                    {
                        hotelService.ListAvailableRooms();
                        break;
                    }
                case "checkout":
                    {
                        string keyNumber = command[1];
                        string guestName = command[2];

                        hotelService.CheckOut(keyNumber, guestName);
                        break;
                    }
                case "list_guest":
                    {
                        hotelService.ListGuest();
                        break;
                    }
                case "get_guest_in_room":
                    {
                        string roomNumber = command[1];

                        hotelService.GetGuestInRoom(roomNumber);
                        break;
                    }
                case "list_guest_by_age":
                    {
                        string option = command[1];
                        int age = int.Parse(command[2]);
                        hotelService.ListGuestByAge(age, option);
                        break;
                    }
                case "list_guest_by_floor":
                    {
                        int floor = int.Parse(command[1]);
                        hotelService.ListGuestByFloor(floor);
                        break;
                    }
                case "checkout_guest_by_floor":
                    {
                        int floor = int.Parse(command[1]);

                        hotelService.CheckOutGuestByFloor(floor);
                        break;
                    }
                case "book_by_floor":
                    {
                        Console.WriteLine($"{sequence}.) Unavailable {command[0]} Function");
                        break;
                    }
                default:
                    break;
            }
        }
    }
}
